//
//  Pin.h
//  TestMap
//
//  Created by Alyona Zaikina on 02/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pin : NSObject

@property (nonatomic, strong) NSString *nameStr;
@property (nonatomic, strong) NSString *latitudeStr;
@property (nonatomic, strong) NSString *longitudeStr;
@property (nonatomic, strong) NSString *addressStr;
@property (nonatomic, strong) NSString *commentStr;
@property (nonatomic, strong) NSString *ratingStr;

@end
