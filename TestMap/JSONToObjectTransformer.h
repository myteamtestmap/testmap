//
//  JSONToObjectTransformer.h
//  TestMap
//
//  Created by Alyona Zaikina on 02/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONToObjectTransformer : NSObject

+ (NSArray *)transformPinsFromJSONArray:(id)jsonObject;

@end
