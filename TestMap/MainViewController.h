//
//  MainViewController.h
//  TestMap
//
//  Created by Alyona Zaikina on 01/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "HTTPClient.h"

@interface MainViewController : UIViewController <CLLocationManagerDelegate, GMSMapViewDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@end
