//
//  DetailViewController.m
//  TestMap
//
//  Created by Alyona Zaikina on 03/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.nameLable.text = self.pin.nameStr;
    self.addressLable.text = self.pin.addressStr;
    self.ratingLable.text = self.pin.ratingStr;
    self.commentTextView.text = self.pin.commentStr;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
