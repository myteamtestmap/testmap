//
//  HTTPClient.h
//  TestMap
//
//  Created by Alyona Zaikina on 01/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"


@interface HTTPClient : AFHTTPSessionManager

+ (HTTPClient *)sharedHttpClient;

- (void)getPinsSuccess:(void (^)(NSArray *pinsArr))success failure:(void (^)(NSString *errorString))failure;

@end
