//
//  TestMapMarker.h
//  TestMap
//
//  Created by Alyona Zaikina on 03/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pin.h"
#import <GoogleMaps/GoogleMaps.h>

@interface TestMapMarker : GMSMarker

@property (nonatomic, strong) Pin *pin;

@end
