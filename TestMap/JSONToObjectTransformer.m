//
//  JSONToObjectTransformer.m
//  TestMap
//
//  Created by Alyona Zaikina on 02/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import "JSONToObjectTransformer.h"
#import "Pin.h"

@implementation JSONToObjectTransformer

+ (NSArray *)transformPinsFromJSONArray:(id)jsonObject{
    NSMutableArray * pins = [[NSMutableArray alloc]init];
    NSDictionary *pinsJSONDic = (NSDictionary*) jsonObject;
    NSArray *pinsJSONArray = [pinsJSONDic objectForKey:@"data"];
    
    for(NSDictionary *pinDict in pinsJSONArray){
        Pin *pin = [[Pin alloc]init];
        pin.nameStr = [pinDict objectForKey:@"name"];
        NSDictionary *locationDic = [pinDict objectForKey:@"location"];
        pin.latitudeStr = [locationDic objectForKey:@"latitude"];
        pin.longitudeStr = [locationDic objectForKey:@"longitude"];
        pin.addressStr = [pinDict objectForKey:@"address"];
        pin.commentStr = [pinDict objectForKey:@"comment"];
        pin.ratingStr = [NSString stringWithFormat:@"%@",[pinDict objectForKey:@"rating"]];
        [pins addObject:pin];
    }
    
    return pins;
}

@end
