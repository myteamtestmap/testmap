//
//  MainViewController.m
//  TestMap
//
//  Created by Alyona Zaikina on 01/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import "MainViewController.h"
#import "AppDelegate.h"
#import "PopoverViewController.h"
#import "TestMapMarker.h"
#import "DetailViewController.h"
#import "Pin.h"

@interface MainViewController ()
@property (nonatomic, strong) PopoverViewController *pinController;
@property (nonatomic, strong) Pin *currentPin;
@end

@implementation MainViewController{
    GMSMapView *mapView_;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:55.38
                                                            longitude:37.97
                                                                 zoom:8];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.myLocationEnabled = YES;
    self.view = mapView_;
    mapView_.delegate = self;
    
    [ApplicationDelegate.client getPinsSuccess:^(NSArray *pinsArr) {
        for (Pin *pin in pinsArr) {
            double latitude = [pin.latitudeStr doubleValue];
            double longitude = [pin.longitudeStr doubleValue];
            TestMapMarker *marker = [TestMapMarker markerWithPosition:CLLocationCoordinate2DMake(latitude, longitude)];
            marker.map = mapView_;
            marker.infoWindowAnchor = CGPointMake(0.44f, 0.45f);
            marker.pin = pin;
        }
    } failure:^(NSString *errorString) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }];
}

#pragma mark - GMSMapViewDelegate
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(id)marker{
    if(self.pinController == nil) {
        self.pinController = [self.storyboard instantiateViewControllerWithIdentifier:@"MapPinPopover"];
    }
    TestMapMarker *curMarker = (TestMapMarker *)marker;
    self.currentPin = curMarker.pin;
    UIView *infoView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 200, 100)];
    [infoView addSubview:self.pinController.view];
    self.pinController.nameLable.text = self.currentPin.nameStr;

    return infoView;
}

-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(TestMapMarker *)marker
{
    [self performSegueWithIdentifier:@"ShowDetailViewSegue" sender:self];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:@"ShowDetailViewSegue"]) {
        DetailViewController *vc = (DetailViewController*)[segue destinationViewController];
        vc.pin = self.currentPin;
    }

}


@end
