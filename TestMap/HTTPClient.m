//
//  HTTPClient.m
//  TestMap
//
//  Created by Alyona Zaikina on 01/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import "HTTPClient.h"
#import "JSONToObjectTransformer.h"

static NSString * const HttpClientURLString = @"http://demo.triptop.co/api";

@implementation HTTPClient

+ (HTTPClient *)sharedHttpClient{
    static HTTPClient *_sharedHttpClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedHttpClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:HttpClientURLString]];
    });
    
    return _sharedHttpClient;
}

- (void)getPinsSuccess:(void (^)(NSArray *pinsArr))success
               failure:(void (^)(NSString *errorString))failure{
    NSDictionary *params = [self defaultMapParamsDic];

    NSString *operationUrl = @"http://demo.triptop.co/api/v1/places/search";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager POST:operationUrl parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSArray *pins = [JSONToObjectTransformer transformPinsFromJSONArray:responseObject];
        success(pins);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        failure(error.localizedDescription);
    }];
}

- (NSDictionary *)defaultMapParamsDic{
    NSDictionary *coordsFromDic = [[NSDictionary alloc]
                                   initWithObjectsAndKeys:@"54.6692642",@"latitude",@"36.765591199999996",@"longitude", nil];
    NSDictionary *coordsToDic = [[NSDictionary alloc]
                                 initWithObjectsAndKeys:@"56.6692642",@"latitude",@"38.765591199999996",@"longitude", nil];
    NSDictionary *params = [[NSDictionary alloc]
                            initWithObjectsAndKeys:coordsFromDic,@"coordsFrom",coordsToDic,@"coordsTo", nil];
    return params;
}

@end
