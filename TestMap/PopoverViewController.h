//
//  PopoverViewController.h
//  TestMap
//
//  Created by Alyona Zaikina on 02/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopoverViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *nameLable;

@end
