//
//  DetailViewController.h
//  TestMap
//
//  Created by Alyona Zaikina on 03/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Pin.h"

@interface DetailViewController : UIViewController

@property (nonatomic, strong) Pin *pin;
@property (nonatomic, weak) IBOutlet UILabel *nameLable;
@property (nonatomic, weak) IBOutlet UILabel *addressLable;
@property (nonatomic, weak) IBOutlet UITextView *commentTextView;
@property (nonatomic, weak) IBOutlet UILabel *ratingLable;

@end
