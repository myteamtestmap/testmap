//
//  AppDelegate.h
//  TestMap
//
//  Created by Alyona Zaikina on 01/12/15.
//  Copyright © 2015 Alyona Zaikina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "HTTPClient.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) HTTPClient *client;


@end

